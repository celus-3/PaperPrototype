# PaperPrototype

Celus-3 project paper prototype 

**Impact Map** https://miro.com/app/board/o9J_kt5-Ro4=/

First of all, let’s answer the question “why?”. Why are we doing this? The main goal is to save engineer’s time. Because the current way to search information is often time-consuming.
Then let’s talk about people. Our team will help Celus to achieve this goal creating necessary software. We will create frontend, backend and database and Celus will get a quick search in PDF. Also, we are going to prepare some technical documentation. In the future we’re ready to maintain our service and implement new improvements. By the end of the third week we will give Celus a presentation of our product and Celus will have the opportunity to test it.
From Celus as a customer our team expect that Celus will share all necessary information and, of corse, give a feedback.
A really important participant in this interaction is a user, especially, electronics engineer who will search information using our service.  So, our team will try to create a product which would be a simple in using and effective in searching.
Admin will help to integrate this service to Celus and support it.

**Story Map** https://miro.com/app/board/o9J_ktGLLz0=/

Who is our user? It’s an electronics engineer who aim a goal to search information by keywords in document uploading it to our service. User has a few main activities related with our service: set up this service, search information with this service, find some additional information somewhere and then use it in our service. 
Before input keywords and get result, user has to choose one of three possible cases: 
1)	Upload a single datasheet;
2)	Choose a certain datasheet from a saved list;
3)	Choose a few datasheets from a saved list.
 After resolving this task, user input keywords, set up a filter on table or figures if he/she needs it and, then, click a button “search”. When user gets result, he or she can start with analytics. 


**Base architecture**

Using our service in a web browser, user uploads or choose from a saved list a certain pdf. This document goes to our File Server. Then we encode this document and it goes to Yandex Vision which returns a payload. We choose special fields depends on user’s keywords and put it in MongoDB. Return a result to user. 

**Detailed backend architecture**

Loaded file is put to file server, next it is encoded in Digitizer to byte format and then is sent to Yandex Vision. 
Yandex Vision returns payload after finding all text in our document. Digitizer gets JSON from Yandex Vision payload which consists from pages which include blocks with coordinates. On the next step these results are filtered and a certain payload is put to MongoDB in required format. 
Search service-converter creates request to database from input request.
Gets from database document ID, pages where necessary information was found, coordinates of the block. And, according to data from the database, the search service-converter searches necessary files. Then it makes crop in necessary file and puts image on file server.

